package it.com.atlassian.bamboo.plugins.docker.pageobjects.api;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.bamboo.utils.BambooPredicates;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.google.common.collect.Iterables;
import it.com.atlassian.bamboo.plugins.docker.pageobjects.AdvancedOptionsElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Map;

import static com.atlassian.bamboo.pageobjects.utils.PageElementFunctions.binder;

public class DockerRunTaskComponent implements TaskComponent
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(DockerRunTaskComponent.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String TASK_NAME = "Docker Run";

    public static final String DOCKER_SERVER_URL = "serverUrl";
    public static final String IMAGE = "image";
    public static final String COMMAND = "command";
    public static final String NAME = "name";

    public static final String CONTAINER_PORT_PREFIX = "containerPort_";
    public static final String HOST_PORT_PREFIX = "hostPort_";

    public static final String CFG_WORKING_SUB_DIRECTORY = "workingSubDirectory";
    public static final String CFG_ENVIRONMENT_VARIABLES = "environmentVariables";

    // ------------------------------------------------------------------------------------------------- Type Properties
    @ElementBy(name = DOCKER_SERVER_URL)
    private TextElement serverUrlField;

    @ElementBy(name = IMAGE)
    private TextElement imageField;

    @ElementBy(name = COMMAND)
    private TextElement commandField;

    @ElementBy(className = "ports-add")
    private PageElement addPortLink;

    @ElementBy(name = NAME)
    private TextElement nameField;

    @ElementBy(name = CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    @ElementBy(cssSelector = "fieldset.collapsible-section")
    private PageElement advancedOptions;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected PageElementFinder elementFinder;

    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void updateTaskDetails(Map<String, String> config)
    {
        getAdvancedOptionsElement().expand();

        if (config.containsKey(DOCKER_SERVER_URL))
        {
            serverUrlField.setText(config.get(DOCKER_SERVER_URL));
        }
        if (config.containsKey(IMAGE))
        {
            imageField.setText(config.get(IMAGE));
        }
        if (config.containsKey(COMMAND))
        {
            commandField.setText(config.get(COMMAND));
        }
        if (config.containsKey(NAME))
        {
            nameField.setText(config.get(NAME));
        }

        for (String containerPortFieldName : Iterables.filter(config.keySet(), BambooPredicates.startsWith(CONTAINER_PORT_PREFIX)))
        {
            addPortLink.click();

            final TextElement containerPortField = elementFinder.find(By.name(containerPortFieldName), TextElement.class);
            containerPortField.setText(config.get(containerPortFieldName));

            final String hostPortFieldName = HOST_PORT_PREFIX + containerPortFieldName.substring(CONTAINER_PORT_PREFIX.length());
            final TextElement hostPortField = elementFinder.find(By.name(hostPortFieldName), TextElement.class);
            hostPortField.setText(config.get(hostPortFieldName));
        }
        if (config.containsKey(CFG_WORKING_SUB_DIRECTORY))
        {
            workingSubDirectoryField.setText(config.get(CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(CFG_ENVIRONMENT_VARIABLES))
        {
            environmentVariablesField.setText(config.get(CFG_ENVIRONMENT_VARIABLES));
        }
    }

    private AdvancedOptionsElement getAdvancedOptionsElement()
    {
        return binder(pageBinder, AdvancedOptionsElement.class).apply(advancedOptions);
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}