package it.com.atlassian.bamboo.plugins.docker.tasks.api;

import com.atlassian.bamboo.pageobjects.pages.deployment.ConfigureDeploymentProjectPage;
import com.atlassian.bamboo.pageobjects.pages.deployment.ConfigureEnvironmentTasksPage;
import com.atlassian.bamboo.pageobjects.pages.deployment.DeploymentResultPage;
import com.atlassian.bamboo.pageobjects.pages.deployment.EnvironmentSummaryPage;
import com.atlassian.bamboo.pageobjects.pages.deployment.execute.ExecuteDeploymentPage;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.model.deployments.projects.TestDeploymentProjectDetails;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.bamboo.plugins.docker.pageobjects.api.DockerRunTaskComponent;
import it.com.atlassian.bamboo.plugins.docker.tasks.AbstractDockerTaskTest;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@Ignore
public class DockerRunTaskTest extends AbstractDockerTaskTest
{
    @Test
    public void testTask() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlanGeneratingArtifacts();
        final TestDeploymentProjectDetails deploymentProject = TestDeploymentProjectDetails.builder()
                .withUniqueName()
                .withPlan(plan)
                .addEnvironment(b -> b.uniqueName().build())
                .build();

        backdoor.deployments().createDeploymentProject(deploymentProject);

        final String deploymentProjectId = deploymentProject.getId();
        final String environmentId = deploymentProject.getEnvironments().get(0).getId();


        final ConfigureDeploymentProjectPage configureDeploymentProjectPage = product.visit(ConfigureDeploymentProjectPage.class, deploymentProjectId);
        final ConfigureEnvironmentTasksPage taskConfigPage = configureDeploymentProjectPage.getEnvironmentComponent(environmentId).editEnvironmentTasks();

        // add Docker task
        final Map<String, String> dockerRunTaskConfig = ImmutableMap.of(DockerRunTaskComponent.DOCKER_SERVER_URL, "http://localhost:2375",
                                                                        DockerRunTaskComponent.IMAGE, "textlab/ubuntu-essential",
                                                                        DockerRunTaskComponent.COMMAND, "touch style.css");
        taskConfigPage.addNewTask(DockerRunTaskComponent.TASK_NAME, DockerRunTaskComponent.class, "Docker run test task", dockerRunTaskConfig);

        product.gotoHomePage();
        backdoor.plans().triggerBuildAndAwaitSuccess(plan.getKey());

        final ExecuteDeploymentPage executeDeploymentPage = product.visit(EnvironmentSummaryPage.class, environmentId).deploy();
        final DeploymentResultPage deploymentResultPage = executeDeploymentPage.withCreateNewRelease().done().submit().waitForDeploymentToFinish();

        assertThat(deploymentResultPage.getStatus(), equalTo("SUCCESS"));
    }
}