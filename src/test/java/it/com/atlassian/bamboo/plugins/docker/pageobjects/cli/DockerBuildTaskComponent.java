package it.com.atlassian.bamboo.plugins.docker.pageobjects.cli;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import it.com.atlassian.bamboo.plugins.docker.pageobjects.AdvancedOptionsElement;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Map;

import static com.atlassian.bamboo.pageobjects.utils.PageElementFunctions.binder;

public class DockerBuildTaskComponent implements TaskComponent
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(DockerBuildTaskComponent.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String TASK_NAME = "Docker";

    public static final String DOCKER_COMMAND_OPTION_BUILD = "build";
    public static final String DOCKERFILE_OPTION_INLINE = "inline";

    public static final String DOCKER_COMMAND_OPTION = "commandOption";
    public static final String DOCKERFILE_OPTION = "dockerfileOption";
    public static final String DOCKERFILE = "dockerfile";
    public static final String REPOSITORY = "repository";
    public static final String NOCACHE = "nocache";
    public static final String SAVE = "save";
    public static final String FILENAME = "filename";

    public static final String CFG_WORKING_SUB_DIRECTORY = "workingSubDirectory";
    public static final String CFG_ENVIRONMENT_VARIABLES = "environmentVariables";
    
    // ------------------------------------------------------------------------------------------------- Type Properties
    @ElementBy(name = DOCKER_COMMAND_OPTION)
    private SelectElement dockerCommandField;

    @ElementBy(name = REPOSITORY)
    private TextElement repositoryField;

    @ElementBy(name = DOCKERFILE)
    private TextElement dockerfileField;

    @ElementBy(name = NOCACHE)
    private CheckboxElement nocacheField;

    @ElementBy(name = SAVE)
    private CheckboxElement saveField;

    @ElementBy(name = FILENAME)
    private TextElement filenameField;

    @ElementBy(name = CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    @ElementBy(id = "advancedOptionsSection")
    private PageElement advancedOptions;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected PageElementFinder elementFinder;

    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void updateTaskDetails(@NotNull final Map<String, String> config)
    {
        getAdvancedOptionsElement().expand();

        dockerCommandField.select(Options.value(DOCKER_COMMAND_OPTION_BUILD));

        if (config.containsKey(REPOSITORY))
        {
            repositoryField.setText(config.get(REPOSITORY));
        }
        if (config.containsKey(DOCKERFILE_OPTION))
        {
            for (PageElement radio : elementFinder.findAll(By.name(DOCKERFILE_OPTION)))
            {
                if (config.get(DOCKERFILE_OPTION).equals(radio.getValue()))
                {
                    radio.click();
                    break;
                }
            }
        }
        if (config.containsKey(DOCKERFILE))
        {
            dockerfileField.setText(config.get(DOCKERFILE));
        }
        if (Boolean.valueOf(config.get(NOCACHE)))
        {
            nocacheField.select();
        }
        if (Boolean.valueOf(config.get(SAVE)))
        {
            saveField.select();
        }
        if (config.containsKey(FILENAME))
        {
            filenameField.setText(config.get(FILENAME));
        }
        if (config.containsKey(CFG_WORKING_SUB_DIRECTORY))
        {
            workingSubDirectoryField.setText(config.get(CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(CFG_ENVIRONMENT_VARIABLES))
        {
            environmentVariablesField.setText(config.get(CFG_ENVIRONMENT_VARIABLES));
        }
    }

    private AdvancedOptionsElement getAdvancedOptionsElement()
    {
        return binder(pageBinder, AdvancedOptionsElement.class).apply(advancedOptions);
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}