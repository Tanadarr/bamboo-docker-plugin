package com.atlassian.bamboo.plugins.docker.client;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.async.ResultCallback;
import com.github.dockerjava.api.command.BuildImageCmd;
import com.github.dockerjava.api.command.CreateContainerCmd;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.InspectContainerCmd;
import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.command.InspectImageCmd;
import com.github.dockerjava.api.command.InspectImageResponse;
import com.github.dockerjava.api.command.PushImageCmd;
import com.github.dockerjava.api.command.RemoveContainerCmd;
import com.github.dockerjava.api.command.StartContainerCmd;
import com.github.dockerjava.api.command.TagImageCmd;
import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.BuildResponseItem;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.api.model.PushResponseItem;
import com.github.dockerjava.api.model.ResponseItem;
import com.github.dockerjava.api.model.Volume;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRule;
import org.mockito.stubbing.Answer;

import javax.annotation.Nullable;
import java.io.File;
import java.util.List;
import java.util.Map;

import static junitparams.JUnitParamsRunner.$;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsArrayContaining.hasItemInArray;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class DockerHttpTest
{
    @Rule
    public MockitoJUnitRule mockitoJUnitRule = new MockitoJUnitRule(this);

    @Mock private DockerClient dockerClient;

    private Docker docker;

    @Before
    public void setUp() throws Exception
    {
        this.docker = new DockerHttp(dockerClient);
    }

    @Test
    public void testRun() throws Exception
    {
        final String imageName = "myImageName";
        final String imageId = "dbec0c020755";
        final String containerId = "511136ea3c5a";

        mockInspectImageCmd(imageName, imageId);
        final CreateContainerCmd createContainerCmd = mockCreateContainerCmd(imageId, containerId);
        final StartContainerCmd startContainerCmd = mockStartContainerCmd(containerId);

        docker.run(imageName, RunConfig.builder().build());

        verify(dockerClient).createContainerCmd(imageId);
        verify(createContainerCmd).exec();
        verify(dockerClient).startContainerCmd(containerId);
        verify(startContainerCmd).exec();
    }

    @Test
    public void testRunCommand() throws Exception
    {
        final String imageName = "myImageName";
        final String imageId = "dbec0c020755";
        final String containerId = "511136ea3c5a";

        mockInspectImageCmd(imageName, imageId);
        final CreateContainerCmd createContainerCmd = mockCreateContainerCmd(imageId, containerId);
        final StartContainerCmd startContainerCmd = mockStartContainerCmd(containerId);

        docker.run(imageName, RunConfig.builder().command("echo \"hello world\"").build());

        verify(dockerClient).createContainerCmd(imageId);
        verify(createContainerCmd).withCmd("echo", "\"hello world\"");
        verify(createContainerCmd).exec();
        verify(dockerClient).startContainerCmd(containerId);
        verify(startContainerCmd).exec();
    }

    @Test
    public void testRunContainerName() throws Exception
    {
        final String imageName = "myImageName";
        final String imageId = "dbec0c020755";
        final String containerId = "511136ea3c5a";
        final String containerName = "myContainerName";

        mockInspectImageCmd(imageName, imageId);
        final CreateContainerCmd createContainerCmd = mockCreateContainerCmd(imageId, containerId);
        final StartContainerCmd startContainerCmd = mockStartContainerCmd(containerId);

        docker.run(imageName, RunConfig.builder().containerName(containerName).build());

        verify(dockerClient).createContainerCmd(imageId);
        verify(createContainerCmd).withName(containerName);
        verify(createContainerCmd).exec();
        verify(dockerClient).startContainerCmd(containerId);
        verify(startContainerCmd).exec();
    }

    @Test
    public void testRunPorts() throws Exception
    {
        final String imageName = "myImageName";
        final String imageId = "dbec0c020755";
        final String containerId = "511136ea3c5a";
        final List<PortMapping> ports = ImmutableList.of(new PortMapping(8080, 8888), new PortMapping(5000, 5000));

        mockInspectImageCmd(imageName, imageId);
        final CreateContainerCmd createContainerCmd = mockCreateContainerCmd(imageId, containerId);
        final StartContainerCmd startContainerCmd = mockStartContainerCmd(containerId);

        docker.run(imageName, RunConfig.builder().ports(ports).build());

        verify(dockerClient).createContainerCmd(imageId);

        ArgumentCaptor<ExposedPort> exposedPortsCaptor = ArgumentCaptor.forClass(ExposedPort.class);
        verify(createContainerCmd).withExposedPorts(exposedPortsCaptor.capture());
        assertThat(exposedPortsCaptor.getAllValues(), containsInAnyOrder(ExposedPort.tcp(8080), ExposedPort.tcp(5000)));

        ArgumentCaptor<Ports> portsCaptor = ArgumentCaptor.forClass(Ports.class);
        verify(createContainerCmd).withPortBindings(portsCaptor.capture());
        verify(createContainerCmd).exec();
        verify(dockerClient).startContainerCmd(containerId);

        assertThat(portsCaptor.getValue().getBindings().get(ExposedPort.tcp(8080)), hasItemInArray(Ports.Binding(8888)));
        assertThat(portsCaptor.getValue().getBindings().get(ExposedPort.tcp(5000)), hasItemInArray(Ports.Binding(5000)));
        
        verify(startContainerCmd).exec();
    }

    @Test
    public void testRunEnv() throws Exception
    {
        final String imageName = "myImageName";
        final String imageId = "dbec0c020755";
        final String containerId = "511136ea3c5a";
        final Map<String, String> env = ImmutableMap.of("GUNICORN_OPTS", "[--preload]", "JAVA_OPTS", "-Xmx256m -Xms128m");

        mockInspectImageCmd(imageName, imageId);
        final CreateContainerCmd createContainerCmd = mockCreateContainerCmd(imageId, containerId);
        final StartContainerCmd startContainerCmd = mockStartContainerCmd(containerId);

        docker.run(imageName, RunConfig.builder().env(env).build());

        String[] expectedEnv = Iterables.toArray(
            Iterables.transform(env.entrySet(), new Function<Map.Entry<String, String>, String>()
            {
                @Override
                public String apply(@Nullable Map.Entry<String, String> env)
                {
                    return env.getKey() + '=' + env.getValue();
                }
            }), String.class);

        verify(dockerClient).createContainerCmd(imageId);
        verify(createContainerCmd).withEnv(expectedEnv);
        verify(createContainerCmd).exec();
        verify(dockerClient).startContainerCmd(containerId);
        verify(startContainerCmd).exec();
    }

    @Test
    public void testRunVolumes() throws Exception
    {
        final String imageName = "myImageName";
        final String imageId = "dbec0c020755";
        final String containerId = "511136ea3c5a";
        final List<DataVolume> volumes = ImmutableList.of(new DataVolume("/data", "/data"), new DataVolume("/test1", "/test2"));

        mockInspectImageCmd(imageName, imageId);
        final CreateContainerCmd createContainerCmd = mockCreateContainerCmd(imageId, containerId);
        final StartContainerCmd startContainerCmd = mockStartContainerCmd(containerId);

        docker.run(imageName, RunConfig.builder().volumes(volumes).build());

        final Iterable<Bind> expectedBinds =
                Iterables.transform(volumes, new Function<DataVolume, Bind>()
                {
                    @Override
                    public Bind apply(@Nullable DataVolume volume)
                    {
                        return new Bind(volume.getHostDirectory(), new Volume(volume.getContainerDataVolume()));
                    }
                });
        final Iterable<Volume> expectedVolumes =
                Iterables.transform(expectedBinds, new Function<Bind, Volume>()
                {
                    @Override
                    public Volume apply(@Nullable Bind bind)
                    {
                        return bind.getVolume();
                    }
                });

        verify(dockerClient).createContainerCmd(imageId);
        verify(createContainerCmd).withVolumes(Iterables.toArray(expectedVolumes, Volume.class));
        verify(createContainerCmd).withBinds(Iterables.toArray(expectedBinds, Bind.class));
        verify(createContainerCmd).exec();
        verify(dockerClient).startContainerCmd(containerId);
        verify(startContainerCmd).exec();
    }

    @Test
    public void testRunWorkDir() throws Exception
    {
        final String imageName = "myImageName";
        final String imageId = "dbec0c020755";
        final String containerId = "511136ea3c5a";
        final String workDir = "/data";

        mockInspectImageCmd(imageName, imageId);
        final CreateContainerCmd createContainerCmd = mockCreateContainerCmd(imageId, containerId);
        final StartContainerCmd startContainerCmd = mockStartContainerCmd(containerId);

        docker.run(imageName, RunConfig.builder().workDir(workDir).build());

        verify(dockerClient).createContainerCmd(imageId);
        verify(createContainerCmd).withWorkingDir(workDir);
        verify(createContainerCmd).exec();
        verify(dockerClient).startContainerCmd(containerId);
        verify(startContainerCmd).exec();
    }

    private void mockInspectImageCmd(String imageName, String imageId)
    {
        final InspectImageCmd inspectImageCmd = mock(InspectImageCmd.class);
        final InspectImageResponse inspectImageResponse = mock(InspectImageResponse.class);
        when(dockerClient.inspectImageCmd(imageName)).thenReturn(inspectImageCmd);
        when(inspectImageCmd.exec()).thenReturn(inspectImageResponse);
        when(inspectImageResponse.getId()).thenReturn(imageId);
    }

    private CreateContainerCmd mockCreateContainerCmd(String imageId, String containerId)
    {
        final CreateContainerCmd createContainerCmd = mock(CreateContainerCmd.class);
        final CreateContainerResponse createContainerResponse = mock(CreateContainerResponse.class);
        when(dockerClient.createContainerCmd(imageId)).thenReturn(createContainerCmd);
        when(createContainerCmd.withEnv(Matchers.<String>anyVararg())).thenReturn(createContainerCmd);
        when(createContainerCmd.exec()).thenReturn(createContainerResponse);
        when(createContainerResponse.getId()).thenReturn(containerId);
        when(createContainerCmd.withPortBindings(any(Ports.class))).thenReturn(createContainerCmd);
        return createContainerCmd;
    }

    private StartContainerCmd mockStartContainerCmd(String containerId)
    {
        final StartContainerCmd startContainerCmd = mock(StartContainerCmd.class);
        when(dockerClient.startContainerCmd(containerId)).thenReturn(startContainerCmd);
        return startContainerCmd;
    }

    @Test
    @Parameters({ "true", "false" })
    public void testIsRunning(boolean isRunning) throws Exception
    {
        final String containerName = "myContainerName";

        final InspectContainerCmd inspectContainerCmd = mock(InspectContainerCmd.class);
        final InspectContainerResponse inspectContainerResponse = mock(InspectContainerResponse.class);
        final InspectContainerResponse.ContainerState containerState = mock(InspectContainerResponse.ContainerState.class);
        when(dockerClient.inspectContainerCmd(containerName)).thenReturn(inspectContainerCmd);
        when(inspectContainerCmd.exec()).thenReturn(inspectContainerResponse);
        when(inspectContainerResponse.getState()).thenReturn(containerState);
        when(containerState.isRunning()).thenReturn(isRunning);

        assertThat(docker.isRunning(containerName), equalTo(isRunning));
    }

    @Test
    public void testGetHostPort() throws Exception
    {
        final String containerName = "myContainerName";
        final Integer hostPort = 47952;
        final Integer containerPort = 5000;

        final InspectContainerCmd inspectContainerCmd = mock(InspectContainerCmd.class);
        final InspectContainerResponse inspectContainerResponse = mock(InspectContainerResponse.class);
        final InspectContainerResponse.NetworkSettings networkSettings = mock(InspectContainerResponse.NetworkSettings.class);
        final Ports ports = mock(Ports.class);
        final Map<ExposedPort, Ports.Binding[]> bindings = ImmutableMap.<ExposedPort, Ports.Binding[]>builder()
                .put(ExposedPort.tcp(containerPort), new Ports.Binding[] { Ports.Binding(hostPort) }).build();

        when(dockerClient.inspectContainerCmd(containerName)).thenReturn(inspectContainerCmd);
        when(inspectContainerCmd.exec()).thenReturn(inspectContainerResponse);
        when(inspectContainerResponse.getNetworkSettings()).thenReturn(networkSettings);
        when(networkSettings.getPorts()).thenReturn(ports);
        when(ports.getBindings()).thenReturn(bindings);

        assertThat(docker.getHostPort(containerName, containerPort), equalTo(hostPort));
    }

    @Test
    public void testRemove() throws Exception
    {
        final String containerName = "myContainerName";

        final RemoveContainerCmd removeContainerCmd = mock(RemoveContainerCmd.class);
        when(dockerClient.removeContainerCmd(containerName)).thenReturn(removeContainerCmd);
        when(removeContainerCmd.withForce()).thenReturn(removeContainerCmd);

        docker.remove(containerName);

        verify(dockerClient).removeContainerCmd(containerName);
        verify(removeContainerCmd).withForce();
        verify(removeContainerCmd).exec();
    }

    @Test
    public void testBuild() throws Exception
    {
        final File dockerFolder = new File("test");
        final String repository = "namespace/repository:tag";

        final BuildImageCmd buildImageCmd = mock(BuildImageCmd.class);
        when(dockerClient.buildImageCmd(any(File.class))).thenReturn(buildImageCmd);
        when(buildImageCmd.withNoCache(anyBoolean())).thenReturn(buildImageCmd);
        when(buildImageCmd.withTag(anyString())).thenReturn(buildImageCmd);
        when(buildImageCmd.exec(Mockito.any())).thenAnswer(successCallback(BuildResponseItem.class));

        docker.build(dockerFolder, repository, BuildConfig.builder().build());

        verify(dockerClient).buildImageCmd(dockerFolder);
        verify(buildImageCmd).withNoCache(false);
        verify(buildImageCmd).withTag(repository);
        verify(buildImageCmd).exec(Mockito.any());
    }

    @Test
    public void testBuildNoCache() throws Exception
    {
        final File dockerFolder = new File("test");
        final String repository = "namespace/repository:tag";

        final BuildImageCmd buildImageCmd = mock(BuildImageCmd.class);
        when(dockerClient.buildImageCmd(any(File.class))).thenReturn(buildImageCmd);
        when(buildImageCmd.withNoCache(anyBoolean())).thenReturn(buildImageCmd);
        when(buildImageCmd.withTag(anyString())).thenReturn(buildImageCmd);
        when(buildImageCmd.exec(Mockito.any())).thenAnswer(successCallback(BuildResponseItem.class));

        docker.build(dockerFolder, repository, BuildConfig.builder().nocache(true).build());

        verify(dockerClient).buildImageCmd(dockerFolder);
        verify(buildImageCmd).withNoCache(true);
        verify(buildImageCmd).withTag(repository);
        verify(buildImageCmd).exec(Mockito.any());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSave() throws Exception
    {
        final String filename = "myRepository.tar";
        final String repository = "namespace/repository:tag";

        docker.save(filename, repository);
    }

    @Test
    public void testTag() throws Exception
    {
        final String image = "namespace/repository:latest";
        final String repository = "namespace/repository";
        final String tag = "v15";

        final TagImageCmd tagImageCmd = mock(TagImageCmd.class);
        when(dockerClient.tagImageCmd(image, repository, tag)).thenReturn(tagImageCmd);

        docker.tag(image, repository, tag);

        verify(dockerClient).tagImageCmd(image, repository, tag);
        verify(tagImageCmd).exec();
    }

    @Test
    public void testPush() throws Exception
    {
        final String repository = "namespace/repository";

        final PushImageCmd pushImageCmd = mock(PushImageCmd.class);
        when(pushImageCmd.exec(Mockito.any())).thenAnswer(successCallback(PushResponseItem.class));
        when(dockerClient.pushImageCmd(repository)).thenReturn(pushImageCmd);

        docker.push(repository, AuthConfig.builder().build());

        verify(dockerClient).pushImageCmd(repository);
        verify(pushImageCmd).exec(Mockito.any());
    }

    @Test
    @Parameters
    public void testPushAuth(AuthConfig authConfig, String registry, String username,
                             String password, String email) throws Exception
    {
        final String repository = "namespace/repository";

        final PushImageCmd pushImageCmd = mock(PushImageCmd.class);
        when(pushImageCmd.exec(Mockito.any())).thenAnswer(successCallback(PushResponseItem.class));
        when(dockerClient.pushImageCmd(repository)).thenReturn(pushImageCmd);
        when(pushImageCmd.withAuthConfig(any(com.github.dockerjava.api.model.AuthConfig.class)))
                .thenReturn(pushImageCmd);

        docker.push(repository, authConfig);

        verify(dockerClient).pushImageCmd(repository);

        ArgumentCaptor<com.github.dockerjava.api.model.AuthConfig> captor = ArgumentCaptor
                .forClass(com.github.dockerjava.api.model.AuthConfig.class);
        verify(pushImageCmd).withAuthConfig(captor.capture());
        assertThat(captor.getValue().getServerAddress(), equalTo(registry));
        assertThat(captor.getValue().getUsername(), equalTo(username));
        assertThat(captor.getValue().getPassword(), equalTo(password));
        assertThat(captor.getValue().getEmail(), equalTo(email));

        verify(pushImageCmd).exec(Mockito.any());
    }

    @NotNull
    private <T extends ResponseItem> Answer<Object> successCallback(final Class<T> responseClass)
    {
        return new Answer<Object>()
        {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {

                final ResultCallback<T> callback = (ResultCallback<T>) invocation.getArguments()[0];
                callback.onNext(mock(responseClass));
                callback.onComplete();
                return null;
            }
        };
    }

    private Object parametersForTestPushAuth()
    {
        final String defaultRegistry = "https://index.docker.io/v1/";
        final String registry = "registry.druidia.com:12345";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";

        return $(
                $(AuthConfig.builder()
                          .username(username)
                          .password(password)
                          .email(email)
                          .build(), defaultRegistry, username, password, email),
                $(AuthConfig.builder()
                          .registryAddress(registry)
                          .username(username)
                          .password(password)
                          .email(email)
                          .build(), registry, username, password, email)
        );
    }
}