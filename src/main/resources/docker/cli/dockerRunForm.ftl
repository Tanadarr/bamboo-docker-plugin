[@s.textfield labelKey='docker.image' name='image' cssClass="long-field" required=true /]

[@s.checkbox labelKey='docker.detach' toggle='true' name='detach' /]
[@ui.bambooSection dependsOn='detach' showOn='true']
    [@s.textfield labelKey='docker.name' name='name' cssClass="long-field" required=true /]
    [@s.label labelKey='docker.ports' cssClass='field-value-normal-font wide' escape=false]
        [@s.param name='value']
            <div class="description">[@s.text name='docker.ports.description' /]</div>
            <table class="docker-plugin__ports aui long-field">
                <thead>
                <tr>
                    <th>[@s.text name="docker.ports.host"/]</th>
                    <th>[@s.text name="docker.ports.container"/]</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr class="docker-plugin__ports-add">
                    <td class="docker-plugin__ports-directory">[@s.textfield name="hostPort" cssClass="text" /]</td>
                    <td class="docker-plugin__ports-container">[@s.textfield name="containerPort" cssClass="text" /]</td>
                    <td class="docker-plugin__ports-actions">
                        <div class="docker-plugin__ports-actions--add aui-button">[@s.text name="global.buttons.add"/]</div>
                    </td>
                </tr>
                    [#list portsIndices?sort as index]
                        [@portSnippet index /]
                    [/#list]
                </tbody>
            </table>
        [/@s.param]
    [/@s.label]
    [@s.checkbox labelKey='docker.service.wait' toggle='true' name='serviceWait' /]
    [@ui.bambooSection dependsOn='serviceWait' showOn='true']
        [@s.textfield labelKey='docker.service.url.pattern' name='serviceUrlPattern' cssClass="long-field" required=true /]
        [@s.textfield labelKey='docker.service.timeout' name="serviceTimeout" required=true /]
    [/@ui.bambooSection]
[/@ui.bambooSection]
[@s.checkbox labelKey='docker.link' toggle='true' name='link']
    [@s.param name='description']
        [@ui.bambooSection dependsOn='link' showOn='true']
            [#if detachedContainers.isEmpty()]
                [@s.text name='docker.link.description.noContainers' /]
            [#else]
                [@s.text name='docker.link.description.containers' /]
                <ul>
                [#list detachedContainers as detachedContainer]
                    <li>${detachedContainer?html}</li>
                [/#list]
                </ul>
            [/#if]
        [/@ui.bambooSection]
    [/@s.param]
[/@s.checkbox]
[@s.textfield labelKey='docker.env' name='envVars' cssClass="long-field" /]
[@s.textfield labelKey='docker.container.command' name='command' cssClass="long-field" /]
[@s.textfield labelKey='docker.workDir' name='workDir' cssClass="long-field" /]
[@s.textfield labelKey='docker.additionalArgs' name='additionalArgs' cssClass="long-field" /]

[@ui.bambooSection id='volumesSection' titleKey='docker.volumes' collapsible=true isCollapsed=(volumesIndices?size == 0)]
<div class="description">[@s.text name='docker.volumes.description' /]</div>
<table class="docker-plugin__volumes aui">
    <thead>
        <tr>
            <th>[@s.text name="docker.volumes.hostDirectory"/]</th>
            <th>[@s.text name="docker.volumes.containerDataVolume"/]</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr class="docker-plugin__volumes-add">
            <td class="docker-plugin__volumes-directory">[@s.textfield name="hostDirectory" cssClass="text" /]</td>
            <td class="docker-plugin__volumes-container">[@s.textfield name="containerDataVolume" cssClass="text" /]</td>
            <td class="docker-plugin__volumes-actions">
                <div class="docker-plugin__volumes-actions--add aui-button">[@s.text name="global.buttons.add"/]</div>
            </td>
        </tr>
    [#list volumesIndices?sort as index]
        [@volumeSnippet index /]
    [/#list]
    </tbody>
</table>
[/@ui.bambooSection]

[#macro portSnippet index=0 hostPort='' containerPort='']
<tr class="docker-plugin__ports-row" data-ports-id="${index}">
    <td class="docker-plugin__ports-host docker-plugin__ports-host--edit">
        [@s.textfield name="hostPort_${index}" cssClass="text short-field"]
            [#if hostPort?has_content][@s.param name='value']${hostPort}[/@s.param][/#if]
        [/@s.textfield]
    </td>
    <td class="docker-plugin__ports-container docker-plugin__ports-container--edit">
        [@s.textfield name="containerPort_${index}" cssClass="text short-field" required=true]
            [#if containerPort?has_content][@s.param name='value']${containerPort}[/@s.param][/#if]
        [/@s.textfield]
    </td>
    <td class="docker-plugin__ports-actions">
        <a class="docker-plugin__ports-actions--remove">[@s.text name="global.buttons.remove" /]</a>
    </td>
</tr>
[/#macro]

<script type="text/x-template" title="ports-list-item-template">
    [#assign portSnippetInst][@portSnippet 869576137068 "869576137069" "869576137070"/][/#assign]
    ${portSnippetInst?replace("869576137068", "{index}")?replace("869576137069", "{hostPort}")?replace("869576137070", "{containerPort}")}
</script>

[#macro volumeSnippet index=0 hostDirectory='' containerDataVolume='']
<tr class="docker-plugin__volumes-row" data-volumes-id="${index}">
    <td class="docker-plugin__volumes-directory">
        [@s.textfield name="hostDirectory_${index}" cssClass="text"]
            [#if hostDirectory?has_content][@s.param name='value']${hostDirectory}[/@s.param][/#if]
        [/@s.textfield]
    </td>
    <td class="docker-plugin__volumes-container docker-plugin__volumes-container--edit">
        [@s.textfield name="containerDataVolume_${index}" cssClass="text" required=true]
            [#if containerDataVolume?has_content][@s.param name='value']${containerDataVolume}[/@s.param][/#if]
        [/@s.textfield]
    </td>
    <td class="docker-plugin__volumes-actions">
        <a class="docker-plugin__volumes-actions--remove">[@s.text name="global.buttons.remove" /]</a>
    </td>
</tr>
[/#macro]

<script type="text/x-template" title="volumes-list-item-template">
    [#assign volumeSnippetInst][@volumeSnippet 548852694237 "548852694238" "548852694239"/][/#assign]
    ${volumeSnippetInst?replace("548852694237", "{index}")?replace("548852694238", "{hostDirectory}")?replace("548852694239", "{containerDataVolume}")}
</script>

<script type="text/javascript">
    BAMBOO.DOCKER.DockerTaskConfiguration.init({
        pluginSelector: '.docker-plugin',
        portsSelector: ".docker-plugin__ports",
        volumesSelector: ".docker-plugin__volumes",
        templates: {
            portsListItem: "ports-list-item-template",
            volumesListItem: "volumes-list-item-template"
        }
    });
</script>