package com.atlassian.bamboo.plugins.docker.validation;

import org.jetbrains.annotations.NotNull;

public interface ConfigValidatorFactory
{
    @NotNull
    ConfigValidator create(@NotNull final String dockerCommandOption);
}
