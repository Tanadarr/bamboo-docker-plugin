package com.atlassian.bamboo.plugins.docker;

public interface PluginConstants
{
    public static final String DOCKER_PLUGIN_KEY = "com.atlassian.bamboo.plugins.bamboo-docker-plugin";
}
