package com.atlassian.bamboo.plugins.docker.client;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Represents a Docker container data volume.
 */
public class DataVolume
{
    private final String containerDataVolume;
    private final String hostDirectory;

    public DataVolume(@NotNull final String containerDataVolume, @Nullable final String hostDirectory)
    {
        this.containerDataVolume = containerDataVolume;
        this.hostDirectory = hostDirectory;
    }

    /**
     * @return the path to the data volume inside the container.
     */
    @NotNull
    public String getContainerDataVolume()
    {
        return containerDataVolume;
    }

    /**
     * @return the path to the mapped host directory. May be null if
     * the container data volume is not mapped to a host directory.
     */
    @Nullable
    public String getHostDirectory()
    {
        return hostDirectory;
    }


    @Override
    public int hashCode()
    {
        return new HashCodeBuilder(671, 13)
                .append(containerDataVolume)
                .append(hostDirectory)
                .toHashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof DataVolume))
        {
            return false;
        }
        DataVolume rhs = (DataVolume) o;
        return new EqualsBuilder()
                .append(containerDataVolume, rhs.containerDataVolume)
                .append(hostDirectory, rhs.hostDirectory)
                .isEquals();
    }
}
