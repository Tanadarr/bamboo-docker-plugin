package com.atlassian.bamboo.plugins.docker.tasks;

import com.atlassian.bamboo.task.TaskDefinition;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class DockerTaskPredicates
{
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    private DockerTaskPredicates()
    {
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods

    /**
     * Get a predicate that returns true if TaskDefinition.getConfiguration contains key and value equal to passed in
     * parameters.
     *
     * @return Predicate object
     */
    public static Predicate<TaskDefinition> isConfigurationFieldEqual(@NotNull final String key, @NotNull final String value)
    {
        return new IsConfigurationFieldEqualPredicate(key, value);
    }

    /**
     * Get a predicate that returns true if TaskDefinition.getConfiguration contains key and value equal to passed in
     * parameters.
     *
     * @return Predicate object
     */
    public static Predicate<TaskDefinition> isConfigurationFieldEqual(@NotNull final String key, final boolean value)
    {
        return new IsConfigurationFieldEqualPredicate(key, Boolean.toString(value));
    }

    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    // ------------------------------------------------------------------------------------------------- Private Classes
    private static class IsConfigurationFieldEqualPredicate implements Predicate<TaskDefinition>
    {
        private final String key;
        private final String value;

        private IsConfigurationFieldEqualPredicate(@NotNull final String key, @NotNull final String value)
        {
            this.key = key;
            this.value = value;
        }

        @Override
        public boolean apply(@Nullable TaskDefinition input)
        {
            final Map<String, String> taskConfig = Preconditions.checkNotNull(input).getConfiguration();
            return taskConfig.containsKey(key) && taskConfig.get(key).equals(value);
        }
    }
}