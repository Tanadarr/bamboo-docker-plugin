package com.atlassian.bamboo.plugins.docker.client;

public class DockerException extends Exception
{
    public DockerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DockerException(String message)
    {
        super(message);
    }

    public DockerException(Throwable e)
    {
        super(e);
    }
}
