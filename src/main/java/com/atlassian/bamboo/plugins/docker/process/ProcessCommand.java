package com.atlassian.bamboo.plugins.docker.process;

import com.atlassian.bamboo.util.PasswordMaskingUtils;
import com.atlassian.fugue.Option;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.List;

/**
 * Represents an external process command. The optional mask value
 * will be hidden when retrieving the "safe" String representation
 * of the command.
 */
@Immutable
public class ProcessCommand
{
    private final List<String> command;
    private final Option<String> mask;

    private ProcessCommand(@NotNull final List<String> command, @NotNull final Option<String> mask)
    {
        this.command = command;
        this.mask = mask;
    }

    /**
     * Get the command as a List. The exact command is returned without
     * applying the mask.
     *
     * @return the unmasked command as a List
     */
    @NotNull
    public List<String> getCommandList()
    {
        return command;
    }

    /**
     * Get the command as a String replacing all occurrences of the mask
     * value with a masked password String, e.g. "********".
     *
     * @return the masked String representation of the command.
     */
    @NotNull
    public String getSafeCommandString()
    {
        final String commandString = Joiner.on(' ').join(command);

        if (mask.isDefined())
        {
            return PasswordMaskingUtils.mask(commandString, mask.get());
        }

        return commandString;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof ProcessCommand))
        {
            return false;
        }
        ProcessCommand pc = (ProcessCommand)o;
        return new EqualsBuilder()
                .append(command, pc.command)
                .append(mask, pc.mask)
                .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder(83, 23)
                .append(command)
                .append(mask)
                .toHashCode();
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public static final class Builder
    {
        private final ImmutableList.Builder<String> commandListBuilder = ImmutableList.<String>builder();
        private Option<String> mask = Option.none();

        public Builder add(String... elements)
        {
            this.commandListBuilder.add(elements);
            return this;
        }

        public Builder addAll(Iterable<String> elements)
        {
            this.commandListBuilder.addAll(elements);
            return this;
        }

        public Builder mask(@NotNull final String mask)
        {
            this.mask = Option.some(mask);
            return this;
        }

        public ProcessCommand build()
        {
            return new ProcessCommand(commandListBuilder.build(), mask);
        }
    }
}