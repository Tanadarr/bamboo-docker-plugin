package com.atlassian.bamboo.plugins.docker.client;

import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.BuildImageCmd;
import com.github.dockerjava.api.command.CreateContainerCmd;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.command.InspectImageResponse;
import com.github.dockerjava.api.command.PushImageCmd;
import com.github.dockerjava.api.command.StartContainerCmd;
import com.github.dockerjava.api.model.AuthConfig;
import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.api.model.Volume;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.command.BuildImageResultCallback;
import com.github.dockerjava.core.command.PushImageResultCallback;
import com.github.dockerjava.jaxrs.DockerCmdExecFactoryImpl;
import com.google.common.base.Function;
import com.google.common.base.Throwables;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.List;
import java.util.Map;

public class DockerHttp implements Docker
{
    private static final String MINIMUM_DOCKER_VERSION = "1.14";

    private final DockerClient docker;

    public DockerHttp(@NotNull final DockerClient docker)
    {
        this.docker = docker;
    }

    public DockerHttp(@NotNull final String dockerServerUrl)
    {
        final DockerClientConfig config = DockerClientConfig.createDefaultConfigBuilder()
                .withVersion(MINIMUM_DOCKER_VERSION)
                .withUri(dockerServerUrl)
                .build();
        this.docker = DockerClientBuilder
                .getInstance(config)
                .withDockerCmdExecFactory(new DockerCmdExecFactoryImpl())
                .build();
    }

    @Override
    public void run(@NotNull final String imageName, @NotNull final RunConfig runConfig)
    {
        final InspectImageResponse inspectImageResponse = docker.inspectImageCmd(imageName).exec();
        final CreateContainerCmd createContainerCmd = docker.createContainerCmd(inspectImageResponse.getId());

        if (runConfig.getContainerName().isDefined())
        {
            createContainerCmd.withName(runConfig.getContainerName().get());
        }

        if (runConfig.getCommand().isDefined())
        {
            final List<String> commands = CommandlineStringUtils.tokeniseCommandline(runConfig.getCommand().get());
            createContainerCmd.withCmd(Iterables.toArray(commands, String.class));
        }

        final Ports portBindings = new Ports();
        final List<ExposedPort> exposedPorts = Lists.newArrayList();
        for (PortMapping port : runConfig.getPorts())
        {
            final ExposedPort containerPort = ExposedPort.tcp(port.getContainerPort());
            exposedPorts.add(containerPort);

            if (port.getHostPort() != null)
            {
                final Ports.Binding hostPort = Ports.Binding(port.getHostPort());
                portBindings.bind(containerPort, hostPort);
            }
        }
        createContainerCmd.withExposedPorts(Iterables.toArray(exposedPorts, ExposedPort.class));

        if (!runConfig.getWorkDir().isEmpty())
        {
            createContainerCmd.withWorkingDir(runConfig.getWorkDir().get());
        }

        final List<Bind> volumeBindings = Lists.newArrayList();
        final List<Volume> volumes = Lists.newArrayList();
        for (DataVolume volume : runConfig.getVolumes())
        {
            final Volume containerDataVolume = new Volume(volume.getContainerDataVolume());
            volumes.add(containerDataVolume);

            if (volume.getHostDirectory() != null)
            {
                final String hostDirectory = volume.getHostDirectory();
                volumeBindings.add(new Bind(hostDirectory, containerDataVolume));
            }
        }
        createContainerCmd.withVolumes(Iterables.toArray(volumes, Volume.class));

        final String[] env = Iterables.toArray(Iterables.transform(runConfig.getEnv().entrySet(), new Function<Map.Entry<String, String>, String>()
        {
            @Override
            public String apply(@Nullable Map.Entry<String, String> env)
            {
                return String.format(EQ_ARG, env.getKey(), env.getValue());
            }
        }), String.class);

        createContainerCmd
                .withEnv(env)
                .withPortBindings(portBindings);

        if (!Iterables.isEmpty(volumeBindings))
        {
            createContainerCmd.withBinds(Iterables.toArray(volumeBindings, Bind.class));
        }

        final CreateContainerResponse createContainerResponse = createContainerCmd.exec();
        final StartContainerCmd startContainerCmd = docker.startContainerCmd(createContainerResponse.getId());

        startContainerCmd.exec();
    }

    @Override
    public boolean isRunning(@NotNull final String containerName) throws DockerException
    {
        final InspectContainerResponse inspectContainerResponse = docker.inspectContainerCmd(containerName).exec();
        return inspectContainerResponse.getState().isRunning();
    }

    @Nullable
    @Override
    public Integer getHostPort(@NotNull final String containerName, @NotNull final Integer containerPort) throws DockerException
    {
        final InspectContainerResponse inspectContainerResponse = docker.inspectContainerCmd(containerName).exec();
        final Map<ExposedPort, Ports.Binding[]> bindings = inspectContainerResponse.getNetworkSettings().getPorts().getBindings();
        final Ports.Binding[] hostPorts = bindings.get(new ExposedPort(containerPort));

        return (hostPorts != null && hostPorts.length > 0) ? hostPorts[0].getHostPort() : null;
    }

    @Override
    public void remove(@NotNull final String containerName) throws DockerException
    {
        docker.removeContainerCmd(containerName).withForce().exec();
    }

    @Override
    public void build(@NotNull final File dockerFolder, @NotNull final String repository,
                      @NotNull final BuildConfig buildConfig) throws DockerException
    {
        final BuildImageCmd buildImageCmd = docker.buildImageCmd(dockerFolder)
                .withNoCache(buildConfig.isNoCache())
                .withTag(repository);
        final BuildImageResultCallback resultCallback = new BuildImageResultCallback();
        buildImageCmd.exec(resultCallback);
        try
        {
            resultCallback.awaitCompletion();
        }
        catch (final InterruptedException e)
        {
            throw Throwables.propagate(e);
        }
    }

    @Override
    public void save(@NotNull String filename, @NotNull String repository) throws DockerException
    {
        throw new UnsupportedOperationException("Docker save api not implemented");
    }

    @Override
    public void tag(@NotNull final String image, @NotNull final String repository,
                    @NotNull final String tag) throws DockerException
    {
        docker.tagImageCmd(image, repository, tag).exec();
    }

    @Override
    public void push(@NotNull final String repository,
                     @NotNull final com.atlassian.bamboo.plugins.docker.client.AuthConfig authorisation)
            throws DockerException
    {
        final PushImageCmd pushImageCmd = docker.pushImageCmd(repository);

        if (authorisation.isAuthorisationProvided())
        {
            final AuthConfig authConfig = new AuthConfig();
            authConfig.setUsername(authorisation.getUsername().get());
            authConfig.setPassword(authorisation.getPassword().get());
            authConfig.setEmail(authorisation.getEmail().get());

            if (authorisation.getRegistryAddress().isDefined())
            {
                authConfig.setServerAddress(authorisation.getRegistryAddress().get());
            }

            pushImageCmd.withAuthConfig(authConfig);
        }

        final PushImageResultCallback resultCallback = new PushImageResultCallback();
        pushImageCmd.exec(resultCallback);
        resultCallback.awaitSuccess();
    }
}
