package com.atlassian.bamboo.plugins.docker.tasks.api;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugins.docker.client.PortMapping;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.utils.BambooPredicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class RunConfiguration
{
    final String dockerServerUrl;
    private final String image;
    private final String command;
    private final String name;
    private final List<PortMapping> ports;
    private final boolean waitForService;
    private final String serviceUrl;
    private final long serviceTimeout;
    private final String environmentVariables;

    @NotNull
    public static RunConfiguration fromContext(@NotNull final CommonTaskContext taskContext)
    {
        return new RunConfiguration(taskContext);
    }

    private RunConfiguration(@NotNull final CommonTaskContext taskContext)
    {
        final ConfigurationMap configurationMap = taskContext.getConfigurationMap();

        dockerServerUrl = configurationMap.get(DockerApiTaskConfigurator.DOCKER_SERVER_URL);
        image = configurationMap.get(DockerApiTaskConfigurator.IMAGE);
        command = configurationMap.get(DockerApiTaskConfigurator.COMMAND);
        name = configurationMap.get(DockerApiTaskConfigurator.NAME);
        ports = getPorts(configurationMap);

        waitForService = configurationMap.getAsBoolean(DockerApiTaskConfigurator.SERVICE_WAIT);
        serviceUrl = getServiceUrl(configurationMap);
        serviceTimeout = getServiceTimeout(configurationMap);

        environmentVariables = configurationMap.get(DockerApiTaskConfigurator.ENV_VARS);
    }

    @NotNull
    private List<PortMapping> getPorts(@NotNull final ConfigurationMap configurationMap)
    {
        final ImmutableList.Builder<PortMapping> ports = ImmutableList.builder();

        for (String key : Iterables.filter(configurationMap.keySet(), BambooPredicates.startsWith(DockerApiTaskConfigurator.CONTAINER_PORT_PREFIX)))
        {
            final Integer containerPort = Integer.parseInt(configurationMap.get(key));
            final String indexString = key.substring(DockerApiTaskConfigurator.CONTAINER_PORT_PREFIX.length());
            final Integer hostPort = Integer.parseInt(configurationMap.get(DockerApiTaskConfigurator.HOST_PORT_PREFIX + indexString));

            ports.add(new PortMapping(containerPort, hostPort));
        }

        return ports.build();
    }

    @Nullable
    private String getServiceUrl(@NotNull final ConfigurationMap configurationMap)
    {
        final String serviceUrlPattern = configurationMap.get(DockerApiTaskConfigurator.SERVICE_URL_PATTERN);
        final String firstHostPort = configurationMap.get(DockerApiTaskConfigurator.FIRST_HOST_PORT);

        if (StringUtils.isBlank(serviceUrlPattern) || StringUtils.isBlank(firstHostPort))
        {
            return serviceUrlPattern;
        }

        return serviceUrlPattern.replace(DockerApiTaskConfigurator.SERVICE_URL_PORT_PLACEHOLDER, firstHostPort);
    }

    private long getServiceTimeout(@NotNull final ConfigurationMap configurationMap)
    {
        final String serviceTimeout = configurationMap.get(DockerApiTaskConfigurator.SERVICE_TIMEOUT);

        if (StringUtils.isBlank(serviceTimeout))
        {
            return DockerApiTaskConfigurator.DEFAULT_TIMEOUT_SECONDS;
        }

        return configurationMap.getAsLong(DockerApiTaskConfigurator.SERVICE_TIMEOUT);
    }

    public String getDockerServerUrl()
    {
        return dockerServerUrl;
    }

    public String getImage()
    {
        return image;
    }

    public String getCommand()
    {
        return command;
    }

    public String getName()
    {
        return name;
    }

    public List<PortMapping> getPorts()
    {
        return ports;
    }

    public boolean isWaitForService()
    {
        return waitForService;
    }

    public String getServiceUrl()
    {
        return serviceUrl;
    }

    public long getServiceTimeout()
    {
        return serviceTimeout;
    }

    public String getEnvironmentVariables()
    {
        return environmentVariables;
    }
}