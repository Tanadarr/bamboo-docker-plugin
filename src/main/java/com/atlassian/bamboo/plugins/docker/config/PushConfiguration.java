package com.atlassian.bamboo.plugins.docker.config;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.task.CommonTaskContext;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES;

public class PushConfiguration
{
    private final String repository;
    private final String username;
    private final String password;
    private final String email;
    private final String environmentVariables;

    @NotNull
    public static PushConfiguration fromContext(@NotNull final CommonTaskContext taskContext)
    {
        return new PushConfiguration(taskContext);
    }

    private PushConfiguration(@NotNull final CommonTaskContext taskContext)
    {
        final ConfigurationMap configurationMap = taskContext.getConfigurationMap();

        repository = configurationMap.get(DockerCliTaskConfigurator.PUSH_REPOSITORY);
        username = configurationMap.get(DockerCliTaskConfigurator.USERNAME);
        password = configurationMap.get(DockerCliTaskConfigurator.PASSWORD);
        email = configurationMap.get(DockerCliTaskConfigurator.EMAIL);

        environmentVariables = configurationMap.get(CFG_ENVIRONMENT_VARIABLES);
    }

    public String getRepository()
    {
        return repository;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    public String getEmail()
    {
        return email;
    }

    public String getEnvironmentVariables()
    {
        return environmentVariables;
    }
}