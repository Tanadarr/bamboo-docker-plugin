package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.plugins.docker.RepositoryKey;
import com.atlassian.bamboo.plugins.docker.RepositoryKeys;
import com.atlassian.bamboo.plugins.docker.client.AuthConfig;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.config.PushConfiguration;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import org.jetbrains.annotations.NotNull;

public class PushService implements DockerService
{
    private final Docker docker;

    public PushService(Docker docker)
    {
        this.docker = docker;
    }

    @Override
    public void execute(@NotNull final CommonTaskContext taskContext) throws TaskException
    {
        final PushConfiguration pushConfig = PushConfiguration.fromContext(taskContext);

        try 
        {
            final RepositoryKey repositoryKey = RepositoryKeys.parseKey(pushConfig.getRepository());

            final AuthConfig authConfig = AuthConfig.builder()
                    .registryAddress(repositoryKey.getRegistry())
                    .username(pushConfig.getUsername())
                    .password(pushConfig.getPassword())
                    .email(pushConfig.getEmail())
                    .build();

            docker.push(repositoryKey.getEncodedRepositoryString(), authConfig);
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }
    }
}