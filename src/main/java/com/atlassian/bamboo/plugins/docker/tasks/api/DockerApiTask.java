package com.atlassian.bamboo.plugins.docker.tasks.api;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plugins.docker.CustomBuildDataHelper;
import com.atlassian.bamboo.plugins.docker.PollingService;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.client.DockerHttp;
import com.atlassian.bamboo.plugins.docker.client.RunConfig;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class DockerApiTask implements TaskType
{
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final PollingService pollingService;

    // ---------------------------------------------------------------------------------------------------- Constructors
    public DockerApiTask(final EnvironmentVariableAccessor environmentVariableAccessor, PollingService pollingService)
    {
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.pollingService = pollingService;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException
    {
        final BuildLogger logger = taskContext.getBuildLogger();
        final RunConfiguration taskConfig = RunConfiguration.fromContext(taskContext);
        final Map<String, String> customData = taskContext.getCommonContext().getCurrentResult().getCustomBuildData();

        final Docker docker = new DockerHttp(taskConfig.getDockerServerUrl());

        try
        {
            logger.addBuildLogEntry(String.format("Running image name (%s)", taskConfig.getImage()));
            docker.run(taskConfig.getImage(), buildRunConfig(taskConfig));

            if (taskConfig.isWaitForService())
            {
                pollingService.waitUntilAvailable(taskConfig.getServiceUrl(), taskConfig.getServiceTimeout(), logger);
                CustomBuildDataHelper.setDeployedServiceUrl(customData, taskConfig.getServiceUrl());
            }

            return TaskResultBuilder.newBuilder(taskContext).build();
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    @NotNull
    private RunConfig buildRunConfig(@NotNull final RunConfiguration config)
    {
        final Map<String, String> extraEnvironmentVariables = environmentVariableAccessor.splitEnvironmentAssignments(
                config.getEnvironmentVariables(), false);

        final RunConfig.Builder runConfig = RunConfig.builder()
                .ports(config.getPorts())
                .env(extraEnvironmentVariables);

        if (StringUtils.isNotBlank(config.getName()))
        {
            runConfig.containerName(config.getName());
        }

        if (StringUtils.isNotBlank(config.getCommand()))
        {
            runConfig.command(config.getCommand());
        }

        return runConfig.build();
    }

    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}