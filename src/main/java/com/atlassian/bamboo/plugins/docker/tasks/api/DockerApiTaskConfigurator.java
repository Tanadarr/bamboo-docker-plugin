package com.atlassian.bamboo.plugins.docker.tasks.api;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.plugins.docker.PluginConstants;
import com.atlassian.bamboo.plugins.docker.tasks.DockerTaskPredicates;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskPredicates;
import com.atlassian.bamboo.util.NumberUtils;
import com.atlassian.bamboo.utils.BambooPredicates;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY;
import static com.atlassian.bamboo.util.NumberUtils.isPositiveInteger;
import static com.atlassian.bamboo.utils.predicates.TextPredicates.startsWith;

public class DockerApiTaskConfigurator extends AbstractTaskConfigurator
{
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String DOCKER_API_TASK_KEY = PluginConstants.DOCKER_PLUGIN_KEY + ":task.docker.api";

    public static final String ENVIRONMENT_ID = "environmentId";
    public static final String TASK_ID = "taskId";

    public static final String DOCKER_SERVER_URL = "serverUrl";
    public static final String IMAGE = "image";
    public static final String COMMAND = "command";
    public static final String NAME = "name";
    public static final String ENV_VARS = "envVars";

    public static final String PORTS_INDICES = "portsIndices";
    public static final String CONTAINER_PORT_PREFIX = "containerPort_";
    public static final String HOST_PORT_PREFIX = "hostPort_";
    public static final String PORT_ERROR_PREFIX = "portError_";

    public static final String SERVICE_WAIT = "serviceWait";
    public static final String SERVICE_URL_PATTERN = "serviceUrlPattern";
    public static final String SERVICE_TIMEOUT = "serviceTimeout";

    public static final String FIRST_HOST_PORT = HOST_PORT_PREFIX + 0;
    public final static String SERVICE_URL_PORT_PLACEHOLDER = "${docker.port}";

    public static final String DEFAULT_CONTAINER_SERVICE_URL = "http://localhost:" + SERVICE_URL_PORT_PLACEHOLDER;
    public static final long DEFAULT_TIMEOUT_SECONDS = 120;

    protected static final Set<String> FIELD_KEYS = ImmutableSet.<String>builder()
            .add(DOCKER_SERVER_URL)
            .add(IMAGE)
            .add(COMMAND)
            .add(NAME)
            .add(ENV_VARS)
            .add(SERVICE_WAIT)
            .add(SERVICE_URL_PATTERN)
            .add(SERVICE_TIMEOUT)
            .add(CFG_ENVIRONMENT_VARIABLES)
            .add(CFG_WORKING_SUB_DIRECTORY)
            .build();

    private static final Map<String, Object> DEFAULT_FIELD_VALUES = ImmutableMap.<String, Object>builder()
            .put(PORTS_INDICES, Collections.EMPTY_LIST)
            .put(SERVICE_URL_PATTERN, DEFAULT_CONTAINER_SERVICE_URL)
            .put(SERVICE_TIMEOUT, DEFAULT_TIMEOUT_SECONDS)
            .build();

    private static String NAME_REGEXP = "[a-zA-Z0-9_.-]*";
    private static final Pattern NAME_PATTERN = Pattern.compile(NAME_REGEXP);

    private static final int MAX_PORT = 65535;

    private final static Predicate<String> isPortField = Predicates.or(
            startsWith(CONTAINER_PORT_PREFIX), startsWith(HOST_PORT_PREFIX));

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private EnvironmentService environmentService;
    private I18nResolver i18nResolver;

    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, @Nullable TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, FIELD_KEYS);

        for (String key : Iterables.filter(params.keySet(), isPortField))
        {
            map.put(key, params.getString(key));
        }

        return map;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.putAll(DEFAULT_FIELD_VALUES);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELD_KEYS);

        context.put(PORTS_INDICES, generatePortsList(taskDefinition));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        if (StringUtils.isBlank(params.getString(DOCKER_SERVER_URL)))
        {
            errorCollection.addError(DOCKER_SERVER_URL, i18nResolver.getText("docker.server.error.empty"));
        }

        if (StringUtils.isBlank(params.getString(IMAGE)))
        {
            errorCollection.addError(IMAGE, i18nResolver.getText("docker.image.error.empty"));
        }

        if (!NAME_PATTERN.matcher(params.getString(NAME)).matches())
        {
            errorCollection.addError(NAME, i18nResolver.getText("docker.name.error.invalid"));
        }

        if (!isNameUniqueInJob(params))
        {
            errorCollection.addError(NAME, i18nResolver.getText("docker.name.error.duplicate"));
        }

        validatePort(params, errorCollection, CONTAINER_PORT_PREFIX);
        final Set<String> hostPorts = validatePort(params, errorCollection, HOST_PORT_PREFIX);

        if (params.getBoolean(SERVICE_WAIT))
        {
            if (hostPorts.size() == 0)
            {
                errorCollection.addError(SERVICE_WAIT, i18nResolver.getText("docker.service.wait.error.ports"));
            }

            if (StringUtils.isBlank(params.getString(SERVICE_URL_PATTERN)))
            {
                errorCollection.addError(SERVICE_URL_PATTERN, i18nResolver.getText("docker.service.url.pattern.error.empty"));
            }

            if (!NumberUtils.isPositiveInteger(params.getString(SERVICE_TIMEOUT)))
            {
                errorCollection.addError(SERVICE_TIMEOUT, i18nResolver.getText("docker.service.timeout.error.invalid"));
            }
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    private boolean isNameUniqueInJob(@NotNull final ActionParametersMap params)
    {
        final long environmentId = params.getLong(ENVIRONMENT_ID, -1);
        final String name = params.getString(NAME);
        final long taskId = params.getLong(TASK_ID, -1);

        final Predicate<TaskDefinition> hasNameNotTaskId = Predicates.and(
                DockerTaskPredicates.isConfigurationFieldEqual(NAME, name),
                Predicates.not(BambooPredicates.hasTaskDefinitionEqualId(taskId)));

        return Iterables.isEmpty(Iterables.filter(getDockerRunTaskDefinitions(environmentId),
                                                  hasNameNotTaskId));
    }

    @NotNull
    private List<TaskDefinition> getDockerRunTaskDefinitions(final long environmentId)
    {
        final Environment environment = environmentService.getEnvironment(environmentId);
        final List<TaskDefinition> taskDefinitions = environment.getTaskDefinitions();

        final Iterable<TaskDefinition> dockerTasks = Iterables.filter(taskDefinitions, TaskPredicates.isTaskDefinitionPluginKeyEqual(DOCKER_API_TASK_KEY));
        return Lists.newArrayList(Iterables.filter(dockerTasks, TaskPredicates.isTaskEnabled()));
    }

    private Set<String> validatePort(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection, @NotNull final String portPrefix)
    {
        final Set<String> definedPorts = Sets.newHashSet();

        for (String key : Iterables.filter(params.keySet(), startsWith(portPrefix)))
        {
            final String port = params.getString(key);
            final String errorField = PORT_ERROR_PREFIX + StringUtils.removeStart(key, portPrefix);

            if (!isPositiveInteger(port) || Integer.parseInt(port) > MAX_PORT)
            {
                errorCollection.addError(errorField, i18nResolver.getText("docker.port.error.invalid", port));
            }
            else if (definedPorts.contains(port))
            {
                errorCollection.addError(errorField, i18nResolver.getText("docker.port.error.duplicate", port));
            }

            definedPorts.add(port);
        }

        return definedPorts;
    }

    private List<Integer> generatePortsList(final TaskDefinition taskDefinition)
    {
        final List<Integer> portsList = Lists.newArrayList();
        for (String key : Iterables.filter(taskDefinition.getConfiguration().keySet(), BambooPredicates.startsWith(CONTAINER_PORT_PREFIX)))
        {
            portsList.add(Integer.valueOf(StringUtils.removeStart(key, CONTAINER_PORT_PREFIX)));
        }
        return portsList;
    }

    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    public void setEnvironmentService(EnvironmentService environmentService)
    {
        this.environmentService = environmentService;
    }

    public void setI18nResolver(I18nResolver i18nResolver)
    {
        this.i18nResolver = i18nResolver;
    }
}