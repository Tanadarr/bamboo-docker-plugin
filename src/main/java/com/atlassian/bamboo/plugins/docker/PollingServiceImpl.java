package com.atlassian.bamboo.plugins.docker;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.google.common.base.Stopwatch;
import com.google.common.util.concurrent.Uninterruptibles;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class PollingServiceImpl implements PollingService
{
    @Override
    public void waitUntilAvailable(@NotNull final String url, final long timeoutSeconds, @NotNull final BuildLogger logger)
    {
        if (timeoutSeconds > 0)
        {
            logger.addBuildLogEntry(String.format("Checking connectivity to deployed server: %s", url));
            final Stopwatch stopwatch = Stopwatch.createStarted();

            while (true)
            {
                if (isAvailable(url, logger))
                {
                    return;
                }

                if (stopwatch.elapsed(TimeUnit.SECONDS) > timeoutSeconds)
                {
                    logger.addErrorLogEntry(String.format("Could not connect to %s, giving up after %d seconds", url, timeoutSeconds));
                    return;
                }

                Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
            }
        }
    }

    private boolean isAvailable(@NotNull final String url, @NotNull final BuildLogger logger)
    {
        logger.addBuildLogEntry(String.format("Attempting connection to %s", url));

        final HttpClientParams params = new HttpClientParams();
        params.setConnectionManagerTimeout(TimeUnit.SECONDS.toMillis(1));

        try
        {
            final GetMethod getMethod = new GetMethod();
            getMethod.setURI(new URI(url, false));

            final HttpClient client = new HttpClient(params);
            client.executeMethod(getMethod);
            int statusCode = getMethod.getStatusCode();

            if (statusCode == HttpStatus.SC_OK)
            {
                logger.addBuildLogEntry(String.format("Successfully connected to %s", url));
                return true;
            }

            logger.addBuildLogEntry(String.format("Failed connecting to %s returned status code %d, text: %s",
                                                  url, statusCode, getMethod.getStatusText()));
        }
        catch (IOException e)
        {
            logger.addBuildLogEntry(String.format("Failed connecting to %s, error: %s", url, e.getMessage()));
        }

        return false;
    }
}
