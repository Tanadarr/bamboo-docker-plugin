package com.atlassian.bamboo.plugins.docker.client;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.jetbrains.annotations.Nullable;

/**
 * Represents a port exposed by a Docker container.
 */
public class PortMapping
{
    private final int containerPort;
    private final Integer hostPort;

    public PortMapping(final int containerPort, @Nullable final Integer hostPort)
    {
        this.containerPort = containerPort;
        this.hostPort = hostPort;
    }

    /**
     * @return the exposed container port.
     */
    public int getContainerPort()
    {
        return containerPort;
    }

    /**
     * @return the mapped host port. May be null if the
     * host port is to be dynamically bound to the exposed
     * container port.
     */
    @Nullable
    public Integer getHostPort()
    {
        return hostPort;
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder(671, 13)
                .append(containerPort)
                .append(hostPort)
                .toHashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof PortMapping))
        {
            return false;
        }
        PortMapping rhs = (PortMapping) o;
        return new EqualsBuilder()
                .append(containerPort, rhs.containerPort)
                .append(hostPort, rhs.hostPort)
                .isEquals();
    }
}
