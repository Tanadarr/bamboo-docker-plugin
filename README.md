# Docker Bamboo Plugin

Plugin for integrating [Docker][docker] with [Atlassian Bamboo][bamboo]. The plugin provides tasks in Bamboo to build Docker images and run Docker containers as part of your build.

This plugin requires Bamboo version 5.10 or above.

[docker]: https://www.docker.com/
[bamboo]: http://www.atlassian.com/software/bamboo/overview

## Tasks

The Docker Bamboo plugin provides the following tasks in Bamboo:

* Docker

### Using the Docker task

The Docker task uses the Docker command line interface and requires Docker capability on the agent. It can be used in both build and deployment plans and supports the following Docker commands:

#### Build a Docker image

Builds a Docker image based on the specified Dockerfile. The Dockerfile may be provided as an existing file in the task's working directory or defined in the task configuration.

The image is stored in Docker's local image installation directory and can be used by subsequent Docker tasks in the job. You can optionally save the image to a file in the working directory which can then be packaged as a build artifact.

#### Run a Docker container

Starts a Docker container based on the specified image.

The task's working directory is mounted and also used as the Docker container's working directory.

The container is removed on completion of the task unless 'Detach container' is selected in which case it is either removed on completion of the job (build plans), or will remain running (deployment plans). Containers can be linked to detached containers started by preceding tasks in a job by selecting the 'Link to detached containers' option.

#### Push a Docker repository to a Docker registry

Pushes a Docker image to a Docker registry. This may be the central Docker Hub registry or a custom registry.

## Troubleshooting

### No space left on device

Docker stores it's images in a local image installation directory. Over time this directory may grow to consume all of the available disk space. When this occurs you should remove unused images by running the "docker rmi" command.

There are also two open Docker issues affecting disk space that may be relevant. Please see ["Device-mapper does not release free space from removed images"][dockerissue3182] and ["Graph deletes are non-atomic, db refs deleted without deleting on-disk entities"][dockerissue6354] for further information.

[dockerissue3182]: https://github.com/docker/docker/issues/3182
[dockerissue6354]: https://github.com/docker/docker/issues/6354

### Permission denied on files created within a Docker container

Docker runs processes inside containers as the root user which means files created on mounted volumes are owned by the root user and not by the user running the docker command (the bamboo agent user). This may cause an issue if a subsequent task requires access to those files on the host.

Docker plans to allow mapping between container and host users in the future. Until then, you can workaround the issue by changing the owner of the files in the mounted volume to the host user:

* Supply the host user's id and group id to the container by setting the following environment variables in the Docker run task configuration: HOST_UID=$UID HOST_GID=$GID
* Run a script inside the container to change the owner of the files in the mounted volume: chown -R $HOST_UID:$HOST_GID /<path_to_mounted_volume>

## License

Copyright (C) 2013 - 2014 Atlassian Corporation Pty Ltd. Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.